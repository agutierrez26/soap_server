<?php


class MysqlConnect
{
    private $server   = 'localhost';
    private $username = 'syd_lp';
    private $password = 'kX9hYicKUBJQuYKC';
    private $db       = 'syd_lp';
    /*private $server   = '192.168.10.10';
    private $username = 'homestead';
    private $password = 'secret';
    private $db       = 'syd_lp';*/
    //this commen
    private $con      = null;

    private function connect(){
        try{
            $this->con = new mysqli(
                $this->server,
                $this->username,
                $this->password,
                $this->db
            );
            if ($this->con->connect_error) {
                return ['code' => 0, 'msg'=>"Connection failed: " . $this->con->connect_error];
            }
        }catch(Exception $exception){
            return ['code' => 0, 'msg'=>"Connection failed: " . $exception->getMessage()];
        }

        return ['code' => 1, 'msg'=>"Code successful"];
    }

    public function insert($table, array $data){
        $con = $this->connect();
        if($con['code']==1){

            $columns = array_keys($data);
            $values  = array_values($data);
            $sql = "INSERT INTO ${table} (".implode(',',$columns).") VALUES ('".implode("','",$values)."')";

            $code = '';
            try{
                if ($this->con->query($sql) === TRUE) {
                    $code =  ['code'=>1, 'msg' => 'Registro guardado correctamente'];
                } else {
                    $code = ['code' => 0, 'msg' =>"Error: " . $sql . "  -->  " . $this->con->error ];
                }
            }catch (\Exception $e){
                $code = ['code' => 0, 'msg' =>"Error:   " . $e->getMessage() ];
            }
            $this->con->close();
            return $code;
        }else{
            return $con;
        }

    }


}

/*

$sql = "INSERT INTO MyGuests (firstname, lastname, email)
VALUES ('John', 'Doe', 'john@example.com')";

if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();*/