<?php
include 'MysqlConnect.php';

function CustomerCreate($client_number, $creacion_sap = '2020-01-01', $plazo = 'holi'){

    $params = array(
        'client_number' => $client_number,
        'creacion_sap'  => $creacion_sap,
        'plazo'         => $plazo,
        'flags'         => 'new_client',
        'source'        => 'soap'
    );

    if(strlen((string)$params['client_number']) != 8 )
        return [ 'code'=>0, 'msg' =>'El parametro client_number debe ser de 8 caracteres'];

    $conector = new MysqlConnect();

    $resp = $conector->insert(
        'client_numbers_temp',
        $params
    );

    return $resp;
}
