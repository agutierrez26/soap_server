<?php

require 'nusoap.php';
require 'FuctionList.php';
$server = new nusoap_server();
$ns = "urn:qxhangarwsdl";

$server->configureWSDL('qxhangar',$ns);
$server->soap_defencoding = 'UTF-8';
$server->decode_utf8 = false;
$server->schemaTargetNamespace = $ns;
//$servicio->register('MiFuncion',array('miparametro'=>'xsd:string'), array('return'=>'xsd:string'), $ns);

$params = array(
    'client_number' => 'xsd:integer',
    /*'creacion_sap'  => 'xsd:date',
    'plazo'         => 'xs:string',*/
);

$return = array('return'=>'xsd:Array');

$server->register('CustomerCreate', $params, $return , $ns);
/*
function AddClientNumber($client_number, $creacion_sap, $plazo){
    return 'Client_number: '.$client_number. ' $creacion: '.$creacion_sap.' Plazo: '.$plazo;
}*/

$server->service(file_get_contents("php://input"));